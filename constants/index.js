import images from "./images";
import icons from "./icons";
import { COLORS, FONT, SIZES, SHADOWS } from "./theme";

const data1 = [
    {
        "job_id": 1,
        "employer_name": "Employeur 1",
        "job_title": "Job 1",
        "job_publisher": "Publisher 1",
        "job_country": "Fr",
        "type": "Full-time"
    },
    {
        "job_id": 2,
        "employer_name": "Employeur 2",
        "job_title": "Job 2",
        "job_publisher": "Publisher 2",
        "job_country": "Fr",
        "type": "Part-time"
    },
    {
        "job_id": 3,
        "employer_name": "Empl  oyeur 3",
        "job_title": "Job 3",
        "job_publisher": "Publisher 3",
        "job_country": "Fr",
        "type": "Contractor"
    }
];

const data2 = [
    {
        "job_id": 4,
        "employer_name": "Employeur 4",
        "job_title": "Job 4",
        "job_publisher": "Publisher 4",
        "job_country": "Fr",
        "type": "Full-time"
    },
    {
        "job_id": 5,
        "employer_name": "Employeur 5",
        "job_title": "Job 5",
        "job_publisher": "Publisher 5",
        "job_country": "Fr",
        "type": "Part-time"
    },
    {
        "job_id": 6,
        "employer_name": "Empl  oyeur 6",
        "job_title": "Job 6",
        "job_publisher": "Publisher 6",
        "job_country": "Fr",
        "type": "Contractor"
    }
];

export { images, icons, COLORS, FONT, SIZES, SHADOWS, data1, data2 };
