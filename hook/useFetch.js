import { useState, useEffect } from "react";
import { data1, data2 } from "../constants";

const useFetch = (endpoint, query) => {
  let [data, setData] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(null);

  const fetchData = async () => {
    setIsLoading(true);

    try {
      if ("React developer" === query.query) {
        data = data1;
      } else if ("React Native developer" === query.query) {
        data = data2;
      } else {
        data = data1.concat(data2);
      }

      setData(data);
      setIsLoading(false);
    } catch (error) {
      setError(error);
      console.log(error)
    } finally {
      setIsLoading(false);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  const refetch = () => {
    setIsLoading(true);
    fetchData();
  };

  return { data, isLoading, error, refetch };
};

export default useFetch;
